const fs = require('fs');
const mongoose = require('mongoose');
const colors = require('colors');
const dotenv = require('dotenv');

// Load Env Vars
dotenv.config({ path: './config/config.env' });

// Load Models
//const Post = require('./models/Post');
const User = require('./models/User');

// Connect to database
mongoose.connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
});

// Read JSON files
// const posts = JSON.parse(
//     fs.readFileSync(`${ __dirname }/_data/posts.json`, 'utf-8')
// );
const users = JSON.parse(
    fs.readFileSync(`${ __dirname }/_data/users.json`, 'utf-8')
);

const importData = async () => {
    try {
        //await Post.create(posts);
        await User.create(users);

        console.log(`data imported...`.green.inverse)
        process.exit();
    } catch (err) {
        console.error(err)
    }
}

const deleteData = async () => {
    try {
        //await Post.deleteMany();
        await User.deleteMany();

        console.log(`data destroyed...`.red.inverse)
        process.exit();
    } catch (err) {
        console.error(err)
    }
}

if (process.argv[2] === '-i') {
    importData();
} else if (process.argv[2] === '-d') {
    deleteData();
}