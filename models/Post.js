const mongoose = require('mongoose');
const slugify = require('slugify');

const PostSchema = new mongoose.Schema({
    title: {
        type: String,
        required: [true, 'Please add a title...'],
        unique: [true, 'Cannot add duplicate titles...'],
        trim: true,
        maxlength: [80, 'Title cannot be more than eighty characters...']
    },
    slug: String,
    subtitle: {
        type: String,
        maxlength: [40, 'Subtitle cannot be more than forty characters.']
    },
    description: {
        type: String,
        required: [true, 'Please add a description'],
        maxlength: [120, 'Description cannot be more than 130 characters.']
    },
    tag: {
        type: [String],
        //required: true,
        enum: [
            'diary',
            'professional',
            'personal',
            'fantasy',
            'science-fiction',
            'reviews',
            'entertainment',
            'industry'
        ]
    },
    content: {
        type: String,
        required: true
    },
    image: {
        type: String,
        default: 'def_post.jpg'
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    relatedwork: {
        /* 
        http://www.foufos.gr
        https://www.foufos.gr
        http://foufos.gr
        http://www.foufos.gr/kino
        http://werer.gr
        www.foufos.gr
        www.mp3.com
        www.t.co
        http://t.co
        http://www.t.co
        https://www.t.co
        www.aa.com
        http://aa.com
        http://www.aa.com
        https://www.aa.com
        */
        type: String,
        match: [/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi, 'Please make sure this link matches secure prefixes...']
    },
    user: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    }
},
{
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
}
);

// Create Post slug from title
PostSchema.pre('save', function (next) {
    this.slug = slugify(this.title, { lower: true });
    next();
})

module.exports = mongoose.model('Post', PostSchema);