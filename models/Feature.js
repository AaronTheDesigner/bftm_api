const mongoose = require('mongoose');
const slugify = require('slugify');

const FeatureSchema = new mongoose.Schema(
    {
        hook: {
            type: String,
            unique: [true, 'Cannot add duplicate titles...'],
            trim: true,
            maxlength: [30, 'Title cannot be more than sixty characters...']
        },
        title: {
            type: String,
            required: [true, 'Please add title...'],
            unique: [true, 'Cannot add duplicate titles...'],
            trim: true,
            maxlength: [60, 'Title cannot be more than sixty characters...']
        },
        slug: String,
        author: {
            type: String,
            required: [true, 'Please add author...'],
            trim: true,
            maxlength: [60, 'Author name cannot be more than sixty characters...']
        },
        image: {
            type: String,
            default: 'def_feature.jpg'
        },
        description: {
            type: String,
            required: [true, 'Please add a description...'],
            trim: true,
            maxlength: [120, 'Description cannot be more than 120 characters...']
        },
        commentary: {
            type: String,
            required: [true, 'Please provide a full commentary of the feature...'],
            trim: true,
            minlength: [1000, 'Full commentary must be more than 1000 characters...']
        },
        tag: {
            type: [String],
            enum: [
                'Short Story',
                'Novella',
                'Novel',
                'Series',
                'Stand-Alone',
                'High Fantasy',
                'Low Fantasy',
                'Sword and Sorcery',
                'Indian/Vedic',
                'American',
                'Native-American',
                'Islamic',
                'European',
                'Ancient',
                'Medeival',
                'Rennaisance',
                'Early-Industrial',
                'Modern'
            ]
        },
        relatedpost: {
            /* 
            http://www.foufos.gr
            https://www.foufos.gr
            http://foufos.gr
            http://www.foufos.gr/kino
            http://werer.gr
            www.foufos.gr
            www.mp3.com
            www.t.co
            http://t.co
            http://www.t.co
            https://www.t.co
            www.aa.com
            http://aa.com
            http://www.aa.com
            https://www.aa.com
            */
            type: String,
            match: [/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi, 'Please make sure this link matches secure prefixes...']
        },
        purchase: {
             /* 
            http://www.foufos.gr
            https://www.foufos.gr
            http://foufos.gr
            http://www.foufos.gr/kino
            http://werer.gr
            www.foufos.gr
            www.mp3.com
            www.t.co
            http://t.co
            http://www.t.co
            https://www.t.co
            www.aa.com
            http://aa.com
            http://www.aa.com
            https://www.aa.com
            */
           type: String,
           match: [/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi, 'Please make sure this link matches secure prefixes...']
        },
        user: {
            type: mongoose.Schema.ObjectId,
            ref: 'User',
            required: true
        }
    },
    {
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    }
)

FeatureSchema.pre('save', function (next) {
    this.slug = slugify(this.title, { lower: true });
    next();
});

module.exports = mongoose.model('Feature', FeatureSchema);