const mongoose = require('mongoose');

const HomepageSchema = new mongoose.Schema(
    {
        header: String,
        sectionOne: String,
        sectionTwo: String,
        sectionThree: String,
        footer: String
    }
)