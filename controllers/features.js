const path = require('path');
const Feature = require('../models/Feature');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const he = require('he');



/**
 * @desc get all features
 * @route get /bftm/v1/features/
 * @access public
 */ 
exports.getFeatures = asyncHandler(async (req, res, next) => {
    res.status(200).json(res.advancedResults)
})


/**
 * @desc get single feature
 * @route get /bftm/v1/features/:id
 * @access public
 */ 
exports.getFeature = asyncHandler(async (req, res, next) => {
   const feature = await Feature.findById(req.params.id);

   const commentary = feature.commentary;
   const decoded = he.decode(commentary);
   feature.commentary = decoded;

   if (!feature) {
       return next(
           new ErrorResponse(`Feature not found with id of ${ req.params.id }`, 404)
       )
   }

   res.status(200).json({
       sucess: true,
       data: feature
   })
})


/**
 * @desc create new feature
 * @route feature /bftm/v1/features/:id
 * @access private
 */ 
exports.createFeature = asyncHandler(async (req, res, next) => {
    // Add user to req body
    req.body.user = req.user.id;

    const feature = await Feature.create(req.body);

    res.status(201).json({
        success: true,
        data: feature
    })
})


/**
 * @desc update feature
 * @route feature /bftm/v1/features/:id
 * @access private
 */ 
exports.updateFeature = asyncHandler(async (req, res, next) => {
    let feature = await Feature.findById(req.params.id);

    // if there is no feature
    if (!feature) {
        return next(
            new ErrorResponse(`Feature not found with id of ${ req.params.id }`, 404)
        )
    }

    // if original poster doesnt match editor or if user role is not contrib or admin
    if (feature.user.toString() !== req.user.id && req.user.role !== 'admin') {
        return next(
            new ErrorResponse(`User ${ req.user.id } is not authorized to update this Feature`, 401)
        );
    }

    feature = await Feature.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true
    });

    res.status(200).json({
        success: true,
        data: feature
    })

})


/**
 * @desc delete feature
 * @route feature /bftm/v1/features/:id
 * @access private
 */ 
exports.deleteFeature = asyncHandler(async (req, res, next) => {
    const feature = await Feature.findById(req.params.id);

    if (!feature) {
        return next(
            new ErrorResponse(`Feature not found with id of ${ req.params.id }`, 404)
        )
    }

    // make sure user is feature owner || make sure user role is admin
    if (feature.user.toString() !== req.user.id || req.user.role !== 'admin' ) {
        return next(
            new ErrorResponse(`User ${ req.user.id } is not authorized to delete this feature`, 401)
        )
    }

    feature.remove();

    res.status(201).json({
        success: true,
        data: feature
    })
})

/**
 * @desc Upload photo for Article
 * @route PUT /api/v1/articles/:id/photo
 * @access Private
 */
exports.featurePhotoUpload = asyncHandler(async (req, res, next) => {
    const feature = await Feature.findById(req.params.id);

    if (!feature) {
        return next(
            new ErrorResponse(`Feature not found with id of ${ req.params.id }`, 404)
        );
    }

    //Make sure user is feature owner || make sure user is admin
    if (feature.user.toString() !== req.user.id || req.user.role !== 'admin') {
        return next(
            new ErrorResponse(
                `User ${ req.user.id } is not authorized to update this feature`,
            )
        )
    }

    if (!req.files) {
        return next(new ErrorResponse(`Please upload a file`, 400))
    }

    const file = req.files.file;

    // Confirm the image is a photo
    if (!file.mimetype.startsWith('image')) {
        return next(new ErrorResponse(`Please upload an image file`, 400))
    }

    // Check File Size
    if (file.size > process.env.MAX_FILE_UPLOAD) {
        return next(
            new ErrorResponse(`Please upload an image less than ${ MAX_FILE_UPLOAD }`, 400)
        )
    }

    // Create custom filename
    file.name = `feat_${ feature._id }${ path.parse(file.name).ext }`;

    file.mv(`${ process.env.FILE_UPLOAD_PATH }/${ file.name }`, async err => {
        if (err) {
            console.log(err);
            new ErrorResponse(`Problem with file upload`, 500);
        }

        await Feature.findByIdAndUpdate(req.params.id, { image: file.name });

        res.status(200).json({
            success: true,
            data: file.name
        });

    });

});
