const User = require('../models/User');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const sendEmail = require('../utils/sendEmail');
const crypto = require('crypto');

/**
 * @desc Register User
 * @route POST /api/v1/auth/register
 * @access Public
 */
exports.register = asyncHandler(async (req, res, next) => {
    // Initialize Body
    const { username, email, password } = req.body;

    // Create User
    const user = await User.create({
        username,
        email,
        password
    });

    // Create token
    sendTokenResponse(user, 200, res);
})


/**
 * @desc Login User
 * @route POST /api/v1/auth/login
 * @access Public
 */
exports.login = asyncHandler(async (req, res, next) => {
    const { email, password } = req.body;

    // Validate Email / Password
    if (!email || !password) {
        return next(new errorResponse(`Please provide an email and password`, 400));
    }

    // Check for User
    const user = await User.findOne({ email }).select('+password');

    if (!user) {
        return next(new errorResponse(`Invalid credentials`, 401));
    }

    // Check Password Match
    const isMatch = await user.matchPassword(password);

    if (!isMatch) {
        return next(new errorResponse(`Invalid credentials`, 401))
    }

    sendTokenResponse(user, 200, res);
})


/**
 * @desc Logout User / Clear Cookie
 * @route GET /api/v1/auth/logout
 * @access Private
 */
exports.logout = asyncHandler(async (req, res, next) => {
    res.cookie('token', 'none', {
        expires: new Date(Date.now() + 10 * 1000),
        httpOnly: true
    });

    res.status(200).json({
        success: true,
        data: {}
    });
});


/**
 * @desc Get Logged User
 * @route GET /api/v1/auth/me
 * @access Private
 */
exports.getMe = asyncHandler(async (req, res, next) => {
    const user = await User.findById(req.user.id);

    res.status(200).json({
        success: true,
        data: user
    });
});


/**
 * @desc Update Details
 * @route PUT /api/v1/auth/update
 * @access Private
 */
exports.updateDetails = asyncHandler(async (req, res, next) => {
    const fieldsToUpdate = {
        username: req.body.username,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        profession: req.body.profession,
        company: req.body.company,
        about: req.body.about,
        newsletter: rew.body.newsletter,
        email: req.body.email
    };

    const user = await User.findByIdAndUpdate(req.user.id, fieldsToUpdate, {
        new: true,
        runValidators: true
    });

    res.status(200).json({
        success: true,
        data: user
    });
});


/**
 * @desc Update Password
 * @route PUT /api/v1/updatepassword
 * @access Private
 */
exports.updatePassword = asyncHandler(async (req, res, next) => {
    const user = await User.findById(req.user.id).select('password');

    //check current password
    if (!(await user.matchPassword(req.body.currentPassword))) {
        return next(new errorResponse('Incorrect Password', 401));
    }

    user.password = req.body.newPassword;

    sendTokenResponse(user, 200, res);

    res.status(200).json({
        success: true,
        data: user
    });

});


/**
 * @desc Forgot Password
 * @route POST /api/v1/auth/forgotpassword
 * @access Public
 */
exports.forgotPassword = asyncHandler(async (req, res, next) => {
    const user = await User.findOne({ email: req.body.email });

    if (!user) {
        return next(new ErrorResponse('There is no user with that email', 404));
    }

    // Get reset token
    const resetToken = user.getResetPasswordToken();

    await user.save({ validateBeforeSave: false });

    // Create reset url
    const resetUrl = `${ req.protocol }://${ req.get(
        'host'
    ) }/bftm/forgotpassword/${ resetToken }`;

    const message = `You are receiving this email because you (or someone else) has requested the reset of a password. Please make a PUT request to: \n\n ${ resetUrl }`;

    try {
        await sendEmail({
            email: user.email,
            subject: 'Password reset token',
            message
        });

        res.status(200).json({ success: true, data: 'Email sent' });
    } catch (err) {
        console.log(err);
        user.resetPasswordToken = undefined;
        user.resetPasswordExpire = undefined;

        await user.save({ validateBeforeSave: false });

        return next(new ErrorResponse('Email could not be sent', 500));
    }

    // res.status(200).json({
    // 	success: true,
    // 	data: user
    // });
});


/**
 * @desc Reset Password
 * @route PUT /api/v1/auth/resetpassword/:resettoken
 * @access Public
 */
exports.resetPassword = asyncHandler(async (req, res, next) => {
    const resetPasswordToken = crypto
        .createHash('sha256')
        .update(req.params.resetToken)
        .digest('hex');

    const user = await User.findOne({
        resetPasswordToken,
        resetPasswordExpire: { $gt: Date.now() }
    });

    if (!user) {
        return next(new ErrorResponse(`Invalid Token`, 400));
    }

    user.password = req.body.password;
    user.resetPasswordToken = undefined;
    user.resetPasswordExpire = undefined;
    await user.save();

    sendTokenResponse(user, 200, res);
});


//__Token__\\

// Get token from model, create cookie and send response
const sendTokenResponse = (user, statusCode, res) => {
    // Create token
    const token = user.getSignedJwtToken();

    const options = {
        expires: new Date(
            Date.now() + process.env.JWT_COOKIE_EXPIRE * 24 * 60 * 60 * 1000
        ),
        httpOnly: true
    };

    if (process.env.NODE_ENV === 'production') {
        options.secure = true;
    }

    res
        .status(statusCode)
        .cookie('token', token, options)
        .json({
            success: true,
            token
        });
};

