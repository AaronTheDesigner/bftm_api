const path = require('path');
const Post = require('../models/Post');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const he = require('he');



/**
 * @desc get all posts
 * @route get /bftm/v1/posts/
 * @access public
 */ 
exports.getPosts = asyncHandler(async (req, res, next) => {
    res.status(200).json(res.advancedResults)
})


/**
 * @desc get single post
 * @route get /bftm/v1/posts/:id
 * @access public
 */ 
exports.getPost = asyncHandler(async (req, res, next) => {
   const post = await Post.findById(req.params.id);

   const content = post.content;
   const decoded = he.decode(content);
   post.content = decoded;

   if (!post) {
       return next(
           new ErrorResponse(`Post not found with id of ${ req.params.id }`, 404)
       )
   }

   res.status(200).json({
       sucess: true,
       data: post
   })
})


/**
 * @desc create new post
 * @route post /bftm/v1/posts/:id
 * @access private
 */ 
exports.createPost = asyncHandler(async (req, res, next) => {
    // Add user to req body
    req.body.user = req.user.id;

    const post = await Post.create(req.body);

    res.status(201).json({
        success: true,
        data: post
    })
})


/**
 * @desc update post
 * @route post /bftm/v1/posts/:id
 * @access private
 */ 
exports.updatePost = asyncHandler(async (req, res, next) => {
    let post = await Post.findById(req.params.id);

    // if there is no post
    if (!post) {
        return next(
            new ErrorResponse(`Post not found with id of ${ req.params.id }`, 404)
        )
    }

    // if original poster doesnt match editor or if user role is not contrib or admin
    if (post.user.toString() !== req.user.id && req.user.role !== 'contributor' || req.user.role !== 'admin') {
        return next(
            new ErrorResponse(`User ${ req.user.id } is not authorized to update this Post`, 401)
        );
    }

    post = await Post.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true
    });

    res.status(200).json({
        success: true,
        data: post
    })

})


/**
 * @desc delete post
 * @route post /bftm/v1/posts/:id
 * @access private
 */ 
exports.deletePost = asyncHandler(async (req, res, next) => {
    const post = await Post.findById(req.params.id);

    if (!post) {
        return next(
            new ErrorResponse(`Post not found with id of ${ req.params.id }`, 404)
        )
    }

    // make sure user is post owner || make sure user role is admin
    if (post.user.toString() !== req.user.id || req.user.role !== 'admin' ) {
        return next(
            new ErrorResponse(`User ${ req.user.id } is not authorized to delete this post`, 401)
        )
    }

    post.remove();

    res.status(201).json({
        success: true,
        data: post
    })
})

/**
 * @desc Upload photo for Article
 * @route PUT /api/v1/articles/:id/photo
 * @access Private
 */
exports.postPhotoUpload = asyncHandler(async (req, res, next) => {
    const post = await Post.findById(req.params.id);

    if (!post) {
        return next(
            new ErrorResponse(`Post not found with id of ${ req.params.id }`, 404)
        );
    }

    //Make sure user is post owner || make sure user is admin
    if (post.user.toString() !== req.user.id || req.user.role !== 'admin') {
        return next(
            new ErrorResponse(
                `User ${ req.user.id } is not authorized to update this post`,
            )
        )
    }

    if (!req.files) {
        return next(new ErrorResponse(`Please upload a file`, 400))
    }

    const file = req.files.file;

    // Confirm the image is a photo
    if (!file.mimetype.startsWith('image')) {
        return next(new ErrorResponse(`Please upload an image file`, 400))
    }

    // Check File Size
    if (file.size > process.env.MAX_FILE_UPLOAD) {
        return next(
            new ErrorResponse(`Please upload an image less than ${ MAX_FILE_UPLOAD }`, 400)
        )
    }

    // Create custom filename
    file.name = `post_${ post._id }${ path.parse(file.name).ext }`;

    file.mv(`${ process.env.FILE_UPLOAD_PATH }/${ file.name }`, async err => {
        if (err) {
            console.log(err);
            new ErrorResponse(`Problem with file upload`, 500);
        }

        await Post.findByIdAndUpdate(req.params.id, { image: file.name });

        res.status(200).json({
            success: true,
            data: file.name
        });

    });

});
