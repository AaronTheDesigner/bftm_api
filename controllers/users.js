const path = require('path');
const User = require('../models/User');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');

/**
 * @desc Get all Users
 * @route GET /api/v1/users
 * @access prospective private
 */
exports.getUsers = asyncHandler(async (req, res, next) => {
    res.status(200).json(res.advancedResults);
})

/**
 * @desc Get single User
 * @route GET /api/v1/users/:id
 * @access prospecive private
 */
exports.getUser = asyncHandler(async (req, res, next) => {
    const user = await User.findById(req.params.id);

    res.status(200).json({
        success: true,
        data: user
    });
});

/**
 * @desc Create User
 * @route POST /api/v1/users
 * @access prospective private
 */
exports.createUser = asyncHandler(async (req, res, next) => {
    const user = await User.create(req.body);

    res.status(201).json({
        success: true,
        data: user
    });
});

/**
 * @desc Update User
 * @route PUT /api/v1/users/:id
 * @access prospective private
 */
exports.updateUser = asyncHandler(async (req, res, next) => {
    const user = await User.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true
    });

    res.status(200).json({
        success: true,
        data: user
    });
});

/**
 * @desc Delete User
 * @route DELETE /api/v1/users/:id
 * @access prospecive private
 */
exports.deleteUser = asyncHandler(async (req, res, next) => {
    const user = await User.findByIdAndDelete(req.params.id);

    res.status(200).json({
        success: true,
        data: {}
    });
});

exports.userPhotoUpload = asyncHandler(async (req, res, next) => {
    const user = await User.findById(req.params.id);

    if (!user) {
        return next(
            new ErrorResponse(`User not found with id of ${ req.params.id }`, 404)
        );
    }

    if (!req.files) {
        return next(new ErrorResponse(`Please upload a file`, 400))
    }

    const file = req.files.file;

    // Confirm the image is a photo
    if (!file.mimetype.startsWith('image')) {
        return next(new ErrorResponse(`Please upload an image file`, 400))
    }

    // Check File Size
    if (file.size > process.env.MAX_FILE_UPLOAD) {
        return next(
            new ErrorResponse(`Please upload an image less than ${ MAX_FILE_UPLOAD }`, 400)
        )
    }

    // Create custom filename
    file.name = `user_${ user._id }${ path.parse(file.name).ext }`;

    console.log(file.name)

    file.mv(`${ process.env.FILE_UPLOAD_PATH }/${ file.name }`, async err => {
        if (err) {
            console.log(err);
            new ErrorResponse(`Problem with file upload`, 500);
        }

        await User.findByIdAndUpdate(req.params.id, { image: file.name });

        res.status(200).json({
            success: true,
            data: file.name
        });

    });

});
