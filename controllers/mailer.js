const MailerLite = require('mailerlite-api-v2-node').default;
const mailer = MailerLite('95bb5ba6a32949e1fbcb50fa7ad48990');
const asyncHandler = require('../middleware/async');

// exports.getAccount = mailer.getAccount().then(account => {
//     return console.log(account)
// }).catch(err => {
//     console.log(err)
// })

exports.getAccount = asyncHandler(async (req, res, next) => {
    const account = await mailer.getAccount()

    res.status(200).json({
        success: true,
        data: account
    });
});

exports.getGroupSubscribers = asyncHandler(async(req, res, next) => {
    const group = await mailer.getGroupSubscribers('7944765');

    res.status(200).json({
        success: true,
        data: group
    });
});

exports.addSubscriber = asyncHandler(async (req, res, next) => {
    const subscriber = await mailer.addSubscriberToGroup('7944765', req.body);
    
    res.status(201).json({
        success: true,
        data: subscriber
    });
});

exports.getFields = asyncHandler(async (req, res, next) => {
    const fields = await mailer.getFields();

    res.status(200).json({
        success: true,
        data: fields
    })
})

// mailer.getGroups().then(res => {
//     console.log(res[0].id)
// })

// mailer.getGroup('7944765').then(res => {
//     console.log(res)
// })

// mailer.addSubscriberToGroup('7944765', { 'email':'aarontoliver@gg.com', 'name':'aaron'}).then(res => {
//     console.log(res)
// })
// .catch(err => console.log(err))
