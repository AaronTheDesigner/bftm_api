const path = require('path');
const Work = require('../models/Work');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const he = require('he');

/**
 * @desc get all works
 * @route get /bftm/v1/works/
 * @access public
 */ 
exports.getWorks = asyncHandler(async (req, res, next) => {
    res.status(200).json(res.advancedResults)
})


/**
 * @desc get single work
 * @route get /bftm/v1/works/:id
 * @access public
 */ 
exports.getWork = asyncHandler(async (req, res, next) => {
   const work = await Work.findById(req.params.id);

   const blurb = work.blurb;
   const decoded = he.decode(blurb);
   work.blurb = decoded;

   if (!work) {
       return next(
           new ErrorResponse(`Work not found with id of ${ req.params.id }`, 404)
       )
   }

   res.status(200).json({
       sucess: true,
       data: work
   })
})


/**
 * @desc create new work
 * @route work /bftm/v1/works/:id
 * @access private
 */ 
exports.createWork = asyncHandler(async (req, res, next) => {
    // Add user to req body
    req.body.user = req.user.id;

    const work = await Work.create(req.body);

    res.status(201).json({
        success: true,
        data: work
    })
})


/**
 * @desc update work
 * @route work /bftm/v1/works/:id
 * @access private
 */ 
exports.updateWork = asyncHandler(async (req, res, next) => {
    let work = await Work.findById(req.params.id);

    // if there is no work
    if (!work) {
        return next(
            new ErrorResponse(`Work not found with id of ${ req.params.id }`, 404)
        )
    }

    // if original poster doesnt match editor or if user role is not admin
    if (work.user.toString() !== req.user.id &&  req.user.role !== 'admin') {
        return next(
            new ErrorResponse(`User ${ req.user.id } is not authorized to update this Work`, 401)
        );
    }

    work = await Work.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true
    });

    res.status(200).json({
        success: true,
        data: work
    })

})


/**
 * @desc delete work
 * @route work /bftm/v1/works/:id
 * @access private
 */ 
exports.deleteWork = asyncHandler(async (req, res, next) => {
    const work = await Work.findById(req.params.id);

    if (!work) {
        return next(
            new ErrorResponse(`Work not found with id of ${ req.params.id }`, 404)
        )
    }

    // make sure user is work owner || make sure user role is admin
    if (work.user.toString() !== req.user.id || req.user.role !== 'admin' ) {
        return next(
            new ErrorResponse(`User ${ req.user.id } is not authorized to delete this work`, 401)
        )
    }

    work.remove();

    res.status(201).json({
        success: true,
        data: work
    })
})

/**
 * @desc Upload photo for Article
 * @route PUT /api/v1/articles/:id/photo
 * @access Private
 */
exports.workPhotoUpload = asyncHandler(async (req, res, next) => {
    const work = await Work.findById(req.params.id);

    if (!work) {
        return next(
            new ErrorResponse(`Work not found with id of ${ req.params.id }`, 404)
        );
    }

    //Make sure user is work owner || make sure user is admin
    if (work.user.toString() !== req.user.id || req.user.role !== 'admin') {
        return next(
            new ErrorResponse(
                `User ${ req.user.id } is not authorized to update this work`,
            )
        )
    }

    if (!req.files) {
        return next(new ErrorResponse(`Please upload a file`, 400))
    }

    const file = req.files.file;

    // Confirm the image is a photo
    if (!file.mimetype.startsWith('image')) {
        return next(new ErrorResponse(`Please upload an image file`, 400))
    }

    // Check File Size
    if (file.size > process.env.MAX_FILE_UPLOAD) {
        return next(
            new ErrorResponse(`Please upload an image less than ${ MAX_FILE_UPLOAD }`, 400)
        )
    }

    // Create custom filename
    file.name = `work_${ work._id }${ path.parse(file.name).ext }`;

    file.mv(`${ process.env.FILE_UPLOAD_PATH }/${ file.name }`, async err => {
        if (err) {
            console.log(err);
            new ErrorResponse(`Problem with file upload`, 500);
        }

        await Work.findByIdAndUpdate(req.params.id, { image: file.name });

        res.status(200).json({
            success: true,
            data: file.name
        });

    });

});
