const path = require('path');
const express = require('express');
const errorHandler = require('./middleware/error')
const dotenv = require('dotenv');
const fileUpload = require('express-fileupload');
const colors = require('colors');
const connectDB = require('./config/db');
const morgan = require('morgan');
const mongoSanitize = require('express-mongo-sanitize');
const helment = require('helmet');
const xss = require('xss-clean');
const rateLimit = require('express-rate-limit');
const hpp = require('hpp');
const cors = require('cors');


// Load Env Vars
dotenv.config({ path: './config/config.env' });

// Route Files
const posts = require('./routes/posts');
const users = require('./routes/users');
const features = require('./routes/features');
const works = require('./routes/works');
const auth = require('./routes/auth');
const mailer = require('./routes/mailer');

//Connect Database
connectDB();

//Initialize Express
const app = express();

// Error Handler
app.use(errorHandler);

// Body Parser
app.use(express.json());

// File Upload
app.use(fileUpload());

// Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));

// Sanitize data
app.use(mongoSanitize());

// Set security headers
app.use(helment());

// Prevent cross site scripting
app.use(xss());

// Rate limiting
const limiter = rateLimit({
    windowMs: 1 * 60 * 100, // 1 mins
    max: 100
});

app.use(limiter);

// Prevent HTTP param solution
app.use(hpp());

// Enable cors
app.use(
    cors({
        credentials: true,
        origin: [
            'http://localhost:3000',
            'http://jayrequard.com'
        ]
    })
);

// Error Handler
app.use(errorHandler);

// Mount Routers
app.use('/bftm/posts', posts);
app.use('/bftm/users', users);
app.use('/bftm/features', features);
app.use('/bftm/works', works);
app.use('/bftm/auth', auth);
app.use('/bftm/mailer', mailer);


// Logging Middleware
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

const PORT = process.env.PORT || 5000;

const server = app.listen( PORT, 
    console.log(`server running in ${ process.env.NODE_ENV } node on port ${ PORT }`.yellow.bold));

// Handle unhandled promise rejections
process.on(`unhandledRejection`, (err, promise) => {
    console.log(`Error: ${ err.message }`.red.bold);
    // Close server $ exit process
    server.close(() => process.exit(1))
})