const express = require('express');
const {
    getUsers,
    getUser,
    createUser,
    updateUser,
    deleteUser,
    userPhotoUpload
} = require('../controllers/users');

const advancedResults = require('../middleware/advancedResults');
const User = require('../models/User');

const router = express.Router({ mergeParams: true });

const { protect, authorize } = require('../middleware/auth');

router
    .route('/')
    .get(advancedResults(User), getUsers)
    .post(protect, authorize('admin', 'contributor', 'user'), createUser);

router
    .route('/:id')
    .get(getUser)
    .put(protect, authorize('admin'), updateUser)
    .delete(protect, authorize('admin'), deleteUser);

router
    .route('/:id/photo')
    .put(userPhotoUpload);

module.exports = router;