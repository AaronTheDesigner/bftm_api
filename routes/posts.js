const express = require('express');
const {
    getPost,
    getPosts,
    createPost,
    updatePost,
    deletePost,
    postPhotoUpload
} = require('../controllers/posts');

const advancedResults = require('../middleware/advancedResults');
const Post = require('../models/Post');

const router = express.Router();

const { protect, authorize } = require('../middleware/auth');

router
    .route('/:id/photo')
    .put(protect, authorize('contributor', 'admin'), postPhotoUpload);

router
    .route('/')
    .get(advancedResults(Post), getPosts)
    .post(protect, authorize('contributor', 'admin'), createPost);

router
    .route('/:id')
    .get(getPost)
    .put(protect, authorize('contributor', 'admin'), updatePost)
    .delete(protect, authorize('contributor', 'admin'), deletePost);

module.exports = router;