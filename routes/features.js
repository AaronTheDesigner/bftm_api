const express = require('express');
const {
    getFeature,
    getFeatures,
    createFeature,
    updateFeature,
    deleteFeature,
    featurePhotoUpload
} = require('../controllers/features');

const advancedResults = require('../middleware/advancedResults');
const Feature = require('../models/Feature');

const router = express.Router();

const { protect, authorize } = require('../middleware/auth');

router
    .route('/:id/photo')
    .put(protect, authorize('admin'), featurePhotoUpload);

router
    .route('/')
    .get(advancedResults(Feature), getFeatures)
    .post(protect, authorize('admin'), createFeature);

router
    .route('/:id')
    .get(getFeature)
    .put(protect, authorize('admin'), updateFeature)
    .delete(protect, authorize('admin'), deleteFeature);

module.exports = router;