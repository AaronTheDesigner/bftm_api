const express = require('express');
const {
    getAccount,
    getGroupSubscribers,
    addSubscriber,
    getFields
} = require('../controllers/mailer')
const router = express.Router();

const { protect, authorize } = require('../middleware/auth');

router
    .route('/')
    .get(protect, authorize('admin'), getAccount)

router
    .route('/subscribers')
    .get(protect, authorize('admin'), getGroupSubscribers)
    .post(addSubscriber)

router
    .route('/fields')
    .get(protect, authorize('admin'), getFields)

module.exports = router;