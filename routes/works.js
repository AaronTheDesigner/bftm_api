const express = require('express');
const {
    getWork,
    getWorks,
    createWork,
    updateWork,
    deleteWork,
    workPhotoUpload
} = require('../controllers/works');

const advancedResults = require('../middleware/advancedResults');
const Work = require('../models/Work');

const router = express.Router();

const { protect, authorize } = require('../middleware/auth');

router
    .route('/:id/photo')
    .put(protect, authorize('admin'), workPhotoUpload);

router
    .route('/')
    .get(advancedResults(Work), getWorks)
    .post(protect, authorize('admin'), createWork);

router
    .route('/:id')
    .get(getWork)
    .put(protect, authorize('admin'), updateWork)
    .delete(protect, authorize('admin'), deleteWork);

module.exports = router;

